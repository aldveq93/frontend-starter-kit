# Starter kit with **gulp** task automation

## To install dependencies, **run** 

``` npm install ```

## To start the automatic tasks and the local server, **run**

``` gulp ```

## Gulp tasks

* SASS to CSS
* Autoprefixer
* Concat CSS
* Minified CSS
* ES6 to ES5 (Babel, Browserify, Babelify)
* Concat JS
* Minified JS
* PUG to HTML5
* BrowserSync

### The archives are compiled to the folder **public**